nix-channel --add https://nixos.org/channels/nixos-${NIXOS_VERSION} nixos
nix-channel --update
echo '{pkgs, ...}: {nix = { package = pkgs.nixUnstable; extraOptions = "experimental-features = nix-command flakes" }}'
nix develop
npm run build
